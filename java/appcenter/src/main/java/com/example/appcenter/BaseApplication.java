package com.example.appcenter;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * author : yanghaozhang
 * date : 2020/11/12
 * description :
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
                if (!(activity instanceof MainActivity)) {
                    Intent intent = activity.getIntent();
                    if (intent != null && intent.hasExtra("title_name")) {
                        activity.setTitle(intent.getStringExtra("title_name"));
                    } else {
                        String name = activity.getClass().getPackage().getName();
                        if (!TextUtils.isEmpty(name)) {
                            String[] nameSplit = name.split("\\.");
                            if (nameSplit != null && nameSplit.length > 0) {
                                activity.setTitle(nameSplit[nameSplit.length - 1]);
                                Log.d("Activity package", "onActivityCreated: " + name);
                            }
                        }
                    }
                }
            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {

            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {

            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });
    }
}
