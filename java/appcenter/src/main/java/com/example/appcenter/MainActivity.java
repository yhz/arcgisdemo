package com.example.appcenter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<ItemBean> mList = new ArrayList<>(165);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new AppCenterAdapter());
    }

    private void initData() {
        mList.add(new ItemBean("add-enc-exchange-set", com.esri.arcgisruntime.sample.addencexchangeset.MainActivity.class));
        mList.add(new ItemBean("add-features-feature-service", com.esri.arcgisruntime.sample.addfeaturesfeatureservice.MainActivity.class));
        mList.add(new ItemBean("add-graphics-renderer", com.esri.arcgisruntime.sample.addgraphicsrenderer.MainActivity.class));
        mList.add(new ItemBean("add-graphics-with-symbols", com.esri.arcgisruntime.sample.addgraphicswithsymbols.MainActivity.class));
        mList.add(new ItemBean("add-point-scene-layer", com.esri.arcgisruntime.sample.addpointscenelayer.MainActivity.class));
        mList.add(new ItemBean("analyze-hotspots", com.esri.arcgisruntime.sample.analyzehotspots.MainActivity.class));
        mList.add(new ItemBean("animate-3d-graphic", com.esri.arcgisruntime.sample.animate3dgraphic.MainActivity.class));
        mList.add(new ItemBean("apply-scheduled-updates-to-preplanned-map-area", com.esri.arcgisruntime.sample.applyscheduledupdatestopreplannedmaparea.MainActivity.class));
        mList.add(new ItemBean("arcgis-map-image-layer-url", com.esri.arcgisruntime.sample.arcgismapimagelayerurl.MainActivity.class));
        mList.add(new ItemBean("arcgis-tiled-layer-url", com.esri.arcgisruntime.sample.arcgistiledlayerurl.MainActivity.class));
        mList.add(new ItemBean("arcgis-vector-tiled-layer-url", com.esri.arcgisruntime.sample.arcgisvectortiledlayerurl.MainActivity.class));
        mList.add(new ItemBean("attribution-view-change", com.esri.arcgisruntime.sample.attributionviewchange.MainActivity.class));
        mList.add(new ItemBean("authenticate-with-oauth", com.esri.arcgisruntime.sample.authenticatewithoauth.MainActivity.class));
        mList.add(new ItemBean("blend-renderer", com.esri.arcgisruntime.sample.blendrenderer.MainActivity.class));
        mList.add(new ItemBean("browse-wfs-layers", com.esri.arcgisruntime.sample.browsewfslayers.MainActivity.class));
        mList.add(new ItemBean("buffer", com.esri.arcgisruntime.sample.buffer.MainActivity.class));
        mList.add(new ItemBean("change-atmosphere-effect", com.esri.arcgisruntime.sample.changeatmosphereeffect.MainActivity.class));
        mList.add(new ItemBean("change-basemaps", com.esri.arcgisruntime.sample.switchbasemaps.MainActivity.class));
        mList.add(new ItemBean("change-feature-layer-renderer", com.esri.arcgisruntime.samples.changefeaturelayerrenderer.MainActivity.class));
        mList.add(new ItemBean("change-sublayer-renderer", com.esri.arcgisruntime.sample.changesublayerrenderer.MainActivity.class));
        mList.add(new ItemBean("change-sublayer-visibility", com.esri.arcgisruntime.sample.mapimagelayersublayervisibility.MainActivity.class));
        mList.add(new ItemBean("change-viewpoint", com.esri.arcgisruntime.sample.changeviewpoint.MainActivity.class));
        mList.add(new ItemBean("choose-camera-controller", com.esri.arcgisruntime.sample.choosecameracontroller.MainActivity.class));
        mList.add(new ItemBean("clip-geometry", com.esri.arcgisruntime.sample.clipgeometry.MainActivity.class));
        mList.add(new ItemBean("colormap-renderer", com.esri.arcgisruntime.sample.colormaprenderer.MainActivity.class));
        mList.add(new ItemBean("control-annotation-sublayer-visibility", com.esri.arcgisruntime.sample.annotationsublayer.MainActivity.class));
        mList.add(new ItemBean("create-and-save-map", com.esri.arcgisruntime.sample.createandsavemap.MainActivity.class));
        mList.add(new ItemBean("create-geometries", com.esri.arcgisruntime.sample.creategeometries.MainActivity.class));
        mList.add(new ItemBean("create-terrain-from-a-local-raster", com.esri.arcgisruntime.samples.createterrainfromalocalraster.MainActivity.class));
        mList.add(new ItemBean("create-terrain-from-a-local-tile-package", com.esri.arcgisruntime.samples.createterrainfromalocaltilepackage.MainActivity.class));
        mList.add(new ItemBean("cut-geometry", com.esri.arcgisruntime.sample.cutgeometry.MainActivity.class));
        mList.add(new ItemBean("delete-features-feature-service", com.esri.arcgisruntime.samples.deletefeaturesfeatureservice.MainActivity.class));
        mList.add(new ItemBean("densify-and-generalize", com.esri.arcgisruntime.sample.densifygeneralize.MainActivity.class));
        mList.add(new ItemBean("dictionary-renderer-graphics-overlay", com.esri.arcgisruntime.sample.dictionaryrenderergraphicsoverlay.MainActivity.class));
        mList.add(new ItemBean("display-device-location", com.esri.arcgisruntime.sample.displaydevicelocation.MainActivity.class));
        mList.add(new ItemBean("display-drawing-status", com.esri.arcgisruntime.sample.displaydrawingstatus.MainActivity.class));
        mList.add(new ItemBean("display-grid", com.esri.arcgisruntime.sample.displaygrid.MainActivity.class));
        mList.add(new ItemBean("display-kml", com.esri.arcgisruntime.sample.displaykml.MainActivity.class));
        mList.add(new ItemBean("display-kml-network-links", com.esri.arcgisruntime.sample.displaykmlnetworklinks.MainActivity.class));
        mList.add(new ItemBean("display-layer-view-state", com.esri.arcgisruntime.sample.displaylayerviewstate.MainActivity.class));
        mList.add(new ItemBean("display-map", com.esri.arcgisruntime.sample.displaymap.MainActivity.class));
        mList.add(new ItemBean("display-scene", com.esri.arcgisruntime.sample.displayscene.MainActivity.class));
        mList.add(new ItemBean("display-scene-in-tabletop-ar", com.esri.arcgisruntime.sample.displayscene.MainActivity.class));
        mList.add(new ItemBean("display-wfs-layer", com.esri.arcgisruntime.sample.displaywfslayer.MainActivity.class));
        mList.add(new ItemBean("distance-composite-symbol", com.esri.arcgisruntime.sample.distancecompositesymbol.MainActivity.class));
        mList.add(new ItemBean("distance-measurement-analysis", com.esri.arcgisruntime.sample.distancemeasurementanalysis.MainActivity.class));
        mList.add(new ItemBean("download-preplanned-map-area", com.esri.arcgisruntime.sample.downloadpreplannedmaparea.MainActivity.class));
        mList.add(new ItemBean("edit-and-sync-features", com.esri.arcgisruntime.sample.editandsyncfeatures.MainActivity.class));
        mList.add(new ItemBean("edit-feature-attachments", com.esri.arcgisruntime.sample.editfeatureattachments.MainActivity.class));
        mList.add(new ItemBean("explore-scene-in-flyover-ar", com.esri.arcgisruntime.sample.exploresceneinflyoverar.MainActivity.class));
        mList.add(new ItemBean("export-tiles", com.esri.arcgisruntime.sample.exporttiles.MainActivity.class));
        mList.add(new ItemBean("feature-collection-layer", com.esri.arcgisruntime.sample.featurecollectionlayer.MainActivity.class));
        mList.add(new ItemBean("feature-collection-layer-query", com.esri.arcgisruntime.sample.featurecollectionlayerquery.MainActivity.class));
        mList.add(new ItemBean("feature-layer-definition-expression", com.esri.arcgisruntime.samples.featurelayerdefinitionexpression.MainActivity.class));
        mList.add(new ItemBean("feature-layer-dictionary-renderer", com.esri.arcgisruntime.sample.featurelayerdictionaryrenderer.MainActivity.class));
        mList.add(new ItemBean("feature-layer-extrusion", com.esri.arcgisruntime.sample.featurelayerextrusion.MainActivity.class));
        mList.add(new ItemBean("feature-layer-feature-service", com.esri.arcgisruntime.sample.featurelayerfeatureservice.MainActivity.class));
        mList.add(new ItemBean("feature-layer-geodatabase", com.esri.arcgisruntime.sample.featurelayergeodatabase.MainActivity.class));
        mList.add(new ItemBean("feature-layer-geopackage", com.esri.arcgisruntime.sample.featurelayergeopackage.MainActivity.class));
        mList.add(new ItemBean("feature-layer-query", com.esri.arcgisruntime.sample.featurelayerquery.MainActivity.class));
        mList.add(new ItemBean("feature-layer-rendering-mode-map", com.esri.arcgisruntime.sample.featurelayerrenderingmodemap.MainActivity.class));
        mList.add(new ItemBean("feature-layer-rendering-mode-scene", com.esri.arcgisruntime.sample.featurelayerrenderingmodescene.MainActivity.class));
        mList.add(new ItemBean("feature-layer-selection", com.esri.arcgisruntime.sample.featurelayerselection.MainActivity.class));
        mList.add(new ItemBean("feature-layer-shapefile", com.esri.arcgisruntime.sample.featurelayershapefile.MainActivity.class));
        mList.add(new ItemBean("feature-layer-show-attributes", com.esri.arcgisruntime.sample.featurelayershowattributes.MainActivity.class));
        mList.add(new ItemBean("feature-layer-update-attributes", com.esri.arcgisruntime.sample.featurelayerupdateattributes.MainActivity.class));
        mList.add(new ItemBean("feature-layer-update-geometry", com.esri.arcgisruntime.sample.featurelayerupdateattributes.MainActivity.class));
        mList.add(new ItemBean("find-address", com.esri.arcgisruntime.sample.findaddress.MainActivity.class));
        mList.add(new ItemBean("find-closest-facility-to-an-incident-interactive", com.esri.arcgisruntime.sample.findclosestfacilitytoanincidentinteractive.MainActivity.class));
        mList.add(new ItemBean("find-closest-facility-to-multiple-incidents-servic", com.esri.arcgisruntime.sample.findclosestfacilitytomultipleincidentsservice.MainActivity.class));
        mList.add(new ItemBean("find-place", com.esri.arcgisruntime.sample.findplace.MainActivity.class));
        mList.add(new ItemBean("find-route", com.esri.arcgisruntime.sample.findroute.MainActivity.class));
        mList.add(new ItemBean("find-service-area-interactive", com.esri.arcgisruntime.sample.findserviceareainteractive.MainActivity.class));
        mList.add(new ItemBean("format-coordinates", com.esri.arcgisruntime.sample.formatcoordinates.MainActivity.class));
        mList.add(new ItemBean("generate-geodatabase", com.esri.arcgisruntime.sample.generategeodatabase.MainActivity.class));
        mList.add(new ItemBean("generate-offline-map", com.esri.arcgisruntime.sample.generateofflinemap.MainActivity.class));
        mList.add(new ItemBean("generate-offline-map-overrides", com.esri.arcgisruntime.generateofflinemapoverrides.MainActivity.class));
        mList.add(new ItemBean("generate-offline-map-with-local-basemap", com.esri.arcgisruntime.sample.generateofflinemapwithlocalbasemap.MainActivity.class));
        mList.add(new ItemBean("geodesic-operations", com.esri.arcgisruntime.geodesicoperations.MainActivity.class));
        mList.add(new ItemBean("get-elevation-at-point", com.esri.arcgisruntime.sample.getelevationatpoint.MainActivity.class));
        mList.add(new ItemBean("group-layers", com.esri.arcgisruntime.sample.grouplayers.MainActivity.class));
        mList.add(new ItemBean("hillshade-renderer", com.esri.arcgisruntime.sample.hillshaderenderer.MainActivity.class));
        mList.add(new ItemBean("honor-mobile-map-package-expiration-date", com.esri.arcgisruntime.sample.honormobilemappackageexpirationdate.MainActivity.class));
        mList.add(new ItemBean("identify-graphics", com.esri.arcgisruntime.sample.identifygraphics.MainActivity.class));
        mList.add(new ItemBean("identify-kml-features", com.esri.arcgisruntime.sample.identifykmlfeatures.MainActivity.class));
        mList.add(new ItemBean("identify-layers", com.esri.arcgisruntime.sample.identifylayers.MainActivity.class));
        mList.add(new ItemBean("integrated-mesh-layer", com.esri.arcgisruntime.sample.integratedmeshlayer.MainActivity.class));
        mList.add(new ItemBean("integrated-windows-authentication", com.esri.arcgisruntime.sample.integratedwindowsauthentication.MainActivity.class));
        mList.add(new ItemBean("line-of-sight-geoelement", com.esri.arcgisruntime.sample.lineofsightgeoelement.MainActivity.class));
        mList.add(new ItemBean("list-kml-contents", com.esri.arcgisruntime.sample.listkmlcontents.MainActivity.class));
        mList.add(new ItemBean("list-related-features", com.esri.arcgisruntime.sample.listrelatedfeatures.MainActivity.class));
        mList.add(new ItemBean("location-line-of-sight", com.esri.arcgisruntime.sample.locationlineofsight.MainActivity.class));
        mList.add(new ItemBean("manage-bookmarks", com.esri.arcgisruntime.sample.managebookmarks.MainActivity.class));
        mList.add(new ItemBean("manage-operational-layers", com.esri.arcgisruntime.sample.manageoperationallayers.MainActivity.class));
        mList.add(new ItemBean("map-image-layer-tables", com.esri.arcgisruntime.sample.mapimagelayertables.MainActivity.class));
        mList.add(new ItemBean("map-load-status", com.esri.arcgisruntime.sample.maploaded.MainActivity.class));
        mList.add(new ItemBean("map-reference-scale", com.esri.arcgisruntime.mapreferencescale.MainActivity.class));
        mList.add(new ItemBean("map-rotation", com.esri.arcgisruntime.sample.maprotation.MainActivity.class));
        mList.add(new ItemBean("mobile-map-search-and-route", com.esri.arcgisruntime.sample.mobilemapsearchandroute.MainActivity.class));
        mList.add(new ItemBean("navigate-in-ar", com.esri.arcgisruntime.sample.navigateinar.MainActivity.class));
        mList.add(new ItemBean("navigate-route", com.esri.arcgisruntime.sample.navigateroute.MainActivity.class));
        mList.add(new ItemBean("offline-geocode", com.esri.arcgisruntime.sample.offlinegeocode.MainActivity.class));
        mList.add(new ItemBean("open-existing-map", com.esri.arcgisruntime.sample.openexistingmap.MainActivity.class));
        mList.add(new ItemBean("open-mobile-map-package", com.esri.arcgisruntime.sample.openmobilemappackage.MainActivity.class));
        mList.add(new ItemBean("open-mobile-scene-package", com.esri.arcgisruntime.sample.openmobilescenepackage.MainActivity.class));
        mList.add(new ItemBean("open-scene-portal-item", com.esri.arcgisruntime.sample.opensceneportalitem.MainActivity.class));
        mList.add(new ItemBean("openstreetmap-layer", com.esri.arcgisruntime.sample.openstreetmaplayer.MainActivity.class));
        mList.add(new ItemBean("perform-spatial-operations", com.esri.arcgisruntime.sample.performspatialoperations.MainActivity.class));
        mList.add(new ItemBean("picture-marker-symbols", com.esri.arcgisruntime.sample.picturemarkersymbols.MainActivity.class));
        mList.add(new ItemBean("play-kml-tour", com.esri.arcgisruntime.sample.playkmltour.MainActivity.class));
        mList.add(new ItemBean("portal-user-info", com.esri.arcgisruntime.sample.portaluserinfo.MainActivity.class));
        mList.add(new ItemBean("project", com.esri.arcgisruntime.sample.project.MainActivity.class));
        mList.add(new ItemBean("query-map-image-sublayer", com.esri.arcgisruntime.sample.querymapimagesublayer.MainActivity.class));
        mList.add(new ItemBean("raster-function-service", com.esri.arcgisruntime.sample.rasterfunctionservice.MainActivity.class));
        mList.add(new ItemBean("raster-layer-file", com.esri.arcgisruntime.sample.rasterlayerfile.MainActivity.class));
        mList.add(new ItemBean("raster-layer-geopackage", com.esri.arcgisruntime.sample.rasterlayergeopackage.MainActivity.class));
        mList.add(new ItemBean("raster-layer-service", com.esri.arcgisruntime.sample.rasterservicelayer.MainActivity.class));
        mList.add(new ItemBean("raster-rendering-rule", com.esri.arcgisruntime.sample.rasterrenderingrule.MainActivity.class));
        mList.add(new ItemBean("read-geopackage", com.esri.arcgisruntime.sample.readgeopackage.MainActivity.class));
        mList.add(new ItemBean("read-symbols-mobile-style-file", com.esri.arcgisruntime.sample.readsymbolsmobilestylefile.MainActivity.class));
        mList.add(new ItemBean("rgb-renderer", com.esri.arcgisruntime.sample.rgbrenderer.MainActivity.class));
        mList.add(new ItemBean("scene-layer", com.esri.arcgisruntime.sample.scenelayer.MainActivity.class));
        mList.add(new ItemBean("scene-layer-selection", com.esri.arcgisruntime.sample.featurelayerselection.MainActivity.class));
        mList.add(new ItemBean("scene-property-expressions", com.esri.arcgisruntime.sample.scenepropertyexpressions.MainActivity.class));
        mList.add(new ItemBean("scene-symbols", com.esri.arcgisruntime.sample.scenesymbols.MainActivity.class));
        mList.add(new ItemBean("search-for-webmap", com.esri.arcgisruntime.sample.searchforwebmap.MainActivity.class));
        mList.add(new ItemBean("service-feature-table-cache", com.esri.arcgisruntime.sample.servicefeaturetablecache.MainActivity.class));
        mList.add(new ItemBean("service-feature-table-manual-cache", com.esri.arcgisruntime.samples.servicefeaturetablemanualcache.MainActivity.class));
        mList.add(new ItemBean("service-feature-table-no-cache", com.esri.arcgisruntime.samples.servicefeaturetablenocache.MainActivity.class));
        mList.add(new ItemBean("set-initial-map-area", com.esri.arcgisruntime.sample.setinitialmaparea.MainActivity.class));
        mList.add(new ItemBean("set-initial-map-location", com.esri.arcgisruntime.sample.setinitialmaplocation.MainActivity.class));
        mList.add(new ItemBean("set-map-spatial-reference", com.esri.arcgisruntime.sample.mapspatialreference.MainActivity.class));
        mList.add(new ItemBean("set-min-max-scale", com.esri.arcgisruntime.samples.setminxmaxscale.MainActivity.class));
        mList.add(new ItemBean("show-callout", com.esri.arcgisruntime.samples.showcallout.MainActivity.class));
        mList.add(new ItemBean("show-labels-on-layer", com.esri.arcgisruntime.showlabelsonlayer.MainActivity.class));
        mList.add(new ItemBean("show-magnifier", com.esri.arcgisruntime.sample.showmagnifier.MainActivity.class));
        mList.add(new ItemBean("simple-marker-symbol", com.esri.arcgisruntime.sample.simplemarkersymbol.MainActivity.class));
        mList.add(new ItemBean("simple-renderer", com.esri.arcgisruntime.sample.simplerenderer.MainActivity.class));
        mList.add(new ItemBean("sketch-editor", com.esri.arcgisruntime.sample.sketcheditor.MainActivity.class));
        mList.add(new ItemBean("spatial-relationships", com.esri.arcgisruntime.sample.spatialrelationships.MainActivity.class));
        mList.add(new ItemBean("statistical-query", com.esri.arcgisruntime.sample.statisticalquery.MainActivity.class));
        mList.add(new ItemBean("statistical-query-group-and-sort", com.esri.arcgisruntime.sample.statisticalquerygroupandsort.MainActivity.class));
        mList.add(new ItemBean("stretch-renderer", com.esri.arcgisruntime.sample.stretchrenderer.MainActivity.class));
        mList.add(new ItemBean("style-wms-layer", com.esri.arcgisruntime.stylewmslayer.MainActivity.class));
        mList.add(new ItemBean("surface-placement", com.esri.arcgisruntime.sample.surfaceplacement.MainActivity.class));
        mList.add(new ItemBean("symbolize-shapefile", com.esri.arcgisruntime.sample.symbolizeshapefile.MainActivity.class));
        mList.add(new ItemBean("sync-map-and-scene-viewpoints", com.esri.arcgisruntime.sample.syncmapandsceneviewpoints.MainActivity.class));
        mList.add(new ItemBean("take-screenshot", com.esri.arcgisruntime.sample.takescreenshot.MainActivity.class));
        mList.add(new ItemBean("terrain-exaggeration", com.esri.arcgisruntime.sample.terrainexaggeration.MainActivity.class));
        mList.add(new ItemBean("time-based-query", com.esri.arcgisruntime.sample.timebasedquery.MainActivity.class));
        mList.add(new ItemBean("token-authentication", com.esri.arcgisruntime.sample.tokenauthentication.MainActivity.class));
        mList.add(new ItemBean("transforms-by-suitability", com.esri.arcgisruntime.sample.transformsbysuitability.MainActivity.class));
        mList.add(new ItemBean("unique-value-renderer", com.esri.arcgisruntime.sample.uniquevaluerenderer.MainActivity.class));
        mList.add(new ItemBean("update-related-features", com.esri.arcgisruntime.sample.updaterelatedfeatures.MainActivity.class));
        mList.add(new ItemBean("view-content-beneath-the-terrain-surface", com.esri.arcgisruntime.sample.viewcontentbeneaththeterrainsurface.MainActivity.class));
        mList.add(new ItemBean("view-point-cloud-data-offline", com.esri.arcgisruntime.sample.viewpointclouddataoffline.MainActivity.class));
        mList.add(new ItemBean("viewshed-camera", com.esri.arcgisruntime.sample.viewshedcamera.MainActivity.class));
        mList.add(new ItemBean("viewshed-geoelement", com.esri.arcgisruntime.sample.viewshedgeoelement.MainActivity.class));
        mList.add(new ItemBean("viewshed-geoprocessing", com.esri.arcgisruntime.sample.viewshedgeoprocessing.MainActivity.class));
        mList.add(new ItemBean("viewshed-location", com.esri.arcgisruntime.sample.viewshedlocation.MainActivity.class));
        mList.add(new ItemBean("web-tiled-layer", com.esri.arcgisruntime.sample.webtiledlayer.MainActivity.class));
        mList.add(new ItemBean("wfs-xml-query", com.esri.arcgisruntime.sample.wfsxmlquery.MainActivity.class));
        mList.add(new ItemBean("wms-layer-url", com.esri.arcgisruntime.sample.wmslayerurl.MainActivity.class));
        mList.add(new ItemBean("wmts-layer", com.esri.arcgisruntime.sample.wmtslayer.MainActivity.class));
    }

    private class AppCenterAdapter extends RecyclerView.Adapter {

        private int selectPosition = -1;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            TextView textView = new TextView(context);
            textView.setPadding(dp2px(context, 10), dp2px(context, 10), 0, dp2px(context, 10));
            textView.setTextSize(dp2px(context, 12));
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            textView.setLayoutParams(layoutParams);
            return new RecyclerView.ViewHolder(textView) {
            };
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((TextView) holder.itemView).setText(mList.get(position).name);
            holder.itemView.setBackgroundColor(position == selectPosition ? Color.YELLOW : Color.TRANSPARENT);
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(MainActivity.this, mList.get(position).clazz);
                intent.putExtra("title_name", mList.get(position).name);
                startActivity(intent);
                selectPosition = position;
                notifyDataSetChanged();
            });
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public int dp2px(Context ctx, float dp) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, ctx.getResources().getDisplayMetrics());
        }
    }

    private static class ItemBean {
        String name;
        Class clazz;

        public ItemBean(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }
    }
}